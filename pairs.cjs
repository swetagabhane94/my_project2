function pairs(obj) {
  if (!obj) {
    return [];
  }
  let result = new Array(3);
  for (let rowIndex = 0; rowIndex < result.length; rowIndex++) {
    result[rowIndex] = new Array(2);
  }
  let colIndex = 0;

  for (let index in obj) {
    result[colIndex++] = [index, obj[index]];
  }
  return result;
}

module.exports = pairs;
