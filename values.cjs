function values(obj) {
  if (!obj) {
    return [];
  }
  let result = [];

  for (let index in obj) {
    if (typeof obj[index] == "function") {
      continue;
    }
    result.push(obj[index]);
  }
  return result;
}

module.exports = values;
