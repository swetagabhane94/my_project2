function invert(obj) {
  if (!obj) {
    return [];
  }
  let result = {};
  for (let index in obj) {
    result[obj[index]] = index;
  }
  return result;
}

module.exports = invert;
