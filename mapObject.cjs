function mapObject(obj, cb) {
  if (!obj) {
    return [];
  }
  let output = {};
  for (let index in obj) {
    output[index] = cb(obj[index], index);
  }
  return output;
}

module.exports = mapObject;
