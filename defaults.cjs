function defaults(obj, defaultProps) {
  if (!obj) {
    return [];
  }
  for (let index in defaultProps) {
    if (!obj[index]) {
      obj[index] = defaultProps[index];
    }
  }
  return obj;
}

module.exports = defaults;
