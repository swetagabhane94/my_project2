function keys(obj) {
  if (!obj) {
    return [];
  }
  let result = [];
  for (let index in obj) {
    result.push(index);
  }
  return result;
}

module.exports = keys;
